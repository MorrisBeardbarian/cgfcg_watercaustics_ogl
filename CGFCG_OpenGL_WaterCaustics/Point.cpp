/*
Written by DSC aprox. 1996
1.7.98 Translation into good-looking C++
*/

// Disable warning for loss of data
//#pragma warning( disable : 4244 )  

#include <malloc.h>
//#include <math.h>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
//#include "text.h"

class Point
{
public:
        double x,y,z;    /* oops... this makes rendering faster! */

        Point();
        Point(double,double,double);
        //Point(text &);
        Point(Point &);

        void create(double,double,double);
        //void load(text &);

        Point operator+(const Point& p);       // addition
        Point operator-(const Point& p);       // substraction
        int operator==(Point);        // comparison
        int operator!=(Point);        // negated comparison                        
        void operator=(const Point& p);        // copy
        Point operator*(double);      // scaling
		Point operator/(double);	  // inv. scaling
        double operator*(const Point& p);      // dot product
        Point operator^(const Point& p);       // cross product

        Point interpolate(Point,double,Point,double);
        double modulo();
        double modulosq();
		double distance(Point);
		double distanceman(Point);
		double distancemanxz(Point);

        void normalize();
        void negate();
        int into(Point, Point);
		float distancePointLine(Point,Point);//distance from point this to line ab

   		void rotatex(double);
		void rotatey(double);
		void rotatey(float,float); //rotatey passing sin(ang) and cos(ang) values
		void rotatez(double);

		bool infrustum(Point,double,double);	// viewer,yaw of viewer,aperture,caller is point to test

		~Point();
};


// Constructors
Point::Point()
{
	x = 0.0;
	y = 0.0;
	z = 0.0;
}


Point::Point(double px, double py, double pz)
{
	x = px;
	y = py;
	z = pz;
}
/*
Point::Point(text &t)
{
x=t.getfloat();
y=t.getfloat();
z=t.getfloat();
}
*/

Point::Point(Point &p)
{
	x = p.x;
	y = p.y;
	z = p.z;
}


// Class methods

void Point::create(double px, double py, double pz)
{
#ifdef DEBUGLOW
	printf("point::create\n");
#endif

	x = px;
	y = py;
	z = pz;
}

/*
void Point::load(text &t)
{
#ifdef DEBUGLOW
printf("point::load\n");
#endif

x=t.getfloat();
y=t.getfloat();
z=t.getfloat();
}
*/
// Operators


double Point::modulo()
{
	double res;

	double tmp = x * x + y * y + z * z;
	res = (double)sqrt((double)tmp);
	return res;
}


double Point::modulosq()
{
	double res;

	res = x * x + y * y + z * z;
	return res;
}


double Point::distance(Point p)
{
	double res;

	res = sqrt((double) ((p.x - x)*(p.x - x) + (p.y - y)*(p.y - y) + (p.z - z)*(p.z - z)) );
	return res;
}



double Point::distanceman(Point p)
{
	double res;

	res = fabs(p.x - x) + fabs(p.y - y) + fabs(p.z - z);
	return res;
}


double Point::distancemanxz(Point p)
{
	double res;

	res = fabs(p.x - x) + fabs(p.z - z);
	return res;
}


void Point::normalize()
{
	double m;

	m = modulo();
	if (m != 0.0)
	{
		x = x / m;
		y = y / m;
		z = z / m;
	}
	else
	{
		x = 0;
		y = 0;
		z = 0;
	}
}


Point Point::operator+(const Point& p)
{
	Point res;


	res.x = x + p.x;
	res.y = y + p.y;
	res.z = z + p.z;
	return res;
}

Point Point::operator-(const Point& p)
{
	Point res;
	res.x = x - p.x;
	res.y = y - p.y;
	res.z = z - p.z;
	return res;
}

int Point::operator==(Point p)
{
	return ((p.x == x) && (p.y == y) && (p.z == z));
}

int Point::operator!=(Point p)
{
	return ((p.x != x) || (p.y != y) || (p.z != z));
}

Point Point::operator*(double d)
{
	Point res;

	res.x = x * d;
	res.y = y * d;
	res.z = z * d;
	return res;
}


Point Point::operator/(double d)
{
	Point res;

	res.x = x / d;
	res.y = y / d;
	res.z = z / d;
	return res;
}


double Point::operator*(const Point& p)
{
	return (p.x*x + p.y*y + p.z*z);
}


Point Point::operator^(const Point& p)
{
	Point res;
	res.x = y * p.z - z * p.y;
	res.y = z * p.x - x * p.z;
	res.z = x * p.y - y * p.x;
	return res;
}


void Point::operator=(const Point& p)
{
	x = p.x;
	y = p.y;
	z = p.z;
}



void Point::negate()
{
	x = -x;
	y = -y;
	z = -z;
}


int Point::into(Point p1, Point p2)
{
	int res;

	res = (x >= p1.x) && (y >= p1.y) && (z >= p1.z) &&
		(x <= p2.x) && (y <= p2.y) && (z <= p2.z);
	return res;
}


Point Point::interpolate(Point p, double fp, Point q, double fq)
{
	Point res;

	res = ((p*fp)) + (q*fq);
	(*this) = res;
	return res;
}


float Point::distancePointLine(Point a, Point b) {
	float distance = 0;
	Point director;
	Point p;
	director = b - a;
	p.create(x, y, z);
	distance = (director ^ (p - a)).modulo() / director.modulo();
	return distance;
}


void Point::rotatex(double angle)
{
	double sinANG, cosANG, tmpY, tmpZ;
	sinANG = sin(angle);
	cosANG = cos(angle);
	tmpY = y;
	tmpZ = z;
	y = tmpY * cosANG + tmpZ * sinANG;
	z = -tmpY * sinANG + tmpZ * cosANG;
}


void Point::rotatey(double angle)
{
	double sinANG, cosANG, tmpX, tmpZ;
	sinANG = sin(angle);
	cosANG = cos(angle);
	tmpX = x;
	tmpZ = z;
	x = tmpX * cosANG - tmpZ * sinANG;
	z = tmpX * sinANG + tmpZ * cosANG;
}


void Point::rotatey(float sin, float cos)
{
	double tmpX, tmpZ;
	tmpX = x;
	tmpZ = z;
	x = tmpX * cos - tmpZ * sin;
	z = tmpX * sin + tmpZ * cos;
}


void Point::rotatez(double angle)
{
	double sinANG, cosANG, tmpX, tmpY;
	sinANG = sin(angle);
	cosANG = cos(angle);
	tmpX = x;
	tmpY = y;
	x = tmpX * cosANG + tmpY * sinANG;
	y = -tmpX * sinANG + tmpY * cosANG;
}


Point::~Point()
{

}


bool Point::infrustum(Point viewer, double yaw, double fov)
{
	Point pyaw(cos(yaw), 0, sin(yaw));
	Point view;
	view = (*this) - viewer;
	view.normalize();
	double cangle = pyaw * view;
	return (cangle > cos(yaw));
}


