#ifndef PLANE_H_
#define PLANE_H_

#include <GL/glew.h>
#include <vector>
#include "Camera.h"
#include <iostream>

// A 3D Plane
class Plane
{
public:

	// Plane Constructor
	Plane(float height, int N, GLuint textureInt);

	// Plane Destructor
	~Plane();

	// Draw the plane
	// params:
	//   shaderProgram - The shader program to draw
	//                   the plane with
	//   cam - The camera (for light position)
	void Draw(GLuint shaderProgram, Camera& cam, int N, GLuint textureInt);

	// The plane texture
	GLuint texture;
	void addNewQuadVertices(std::vector<GLfloat>& vertices, std::vector<GLfloat>& texture, float i, float j, float N, int height);

	GLuint vertexArray;
	GLuint vertexBuffer;
	GLuint textureCoordBuffer;
	GLuint sunTexCoordBuffer;

	std::vector<GLfloat> planeVertices;
	std::vector<GLfloat> planeTextureCoordinates;
private:
	void InitVertices(float height, int N, std::vector<GLfloat>& vertices, std::vector<GLfloat>& texture);
	
};

#endif
