#include "Plane.h"

Plane::Plane(float height, int N, GLuint textureInt)
{
	InitVertices(height, N, planeVertices, planeTextureCoordinates);
	this->texture = textureInt;
}

Plane::~Plane()
{
	glDeleteVertexArrays(1, &this->vertexArray);
	glDeleteBuffers(1, &this->vertexBuffer);
	//glDeleteBuffers(1, &planeTextureCoordinates);
	glDeleteBuffers(1, &sunTexCoordBuffer);
}

void Plane::addNewQuadVertices(std::vector<GLfloat>& vertices, std::vector<GLfloat>& texture, float i, float j, float N, int height) {

	// x y z 
	GLfloat verts[] =
	{
	  i, height, j, //(i/N), (j/N), 
	  (i+1), height, j, //((i+1)/N), (j/N),
	  i, height, (j+1), // (i/N), ((j+1)/N),

	  (i + 1), height, j, // ((i + 1)/N), (j/N),
	  i, height, (j+1), // (i/N), ((j+1)/N),
	  (i+1), height, (j+1) //, ((i+1)/N), ((j+1)/N)
	};

	// u v
	GLfloat tex[] = {
		(i / N), (j / N),
		((i + 1) / N), (j / N),
		(i / N), ((j + 1) / N),

		((i + 1) / N), (j / N),
		(i / N), ((j + 1) / N),
		((i + 1) / N), ((j + 1) / N)
	};

	// for (int k = 0; k < 30; k++) {
	for (int k = 0; k < 18; k++) {
		vertices.push_back(verts[k]);
	}

	for (int k = 0; k < 12; k++) {
		texture.push_back(tex[k]);
	}
}


void Plane::InitVertices(float height, int N, std::vector<GLfloat>& vertices, std::vector<GLfloat>& textureCoords)
{
	float n_fl = (float)N;

	for (int j = 0; j < N; j++)
		for (int i = 0; i < N; i++)
		{
			// convert i, j and N to float
			float i_fl = (float)i;
			float j_fl = (float)j;

			// append new quad
			addNewQuadVertices(vertices, textureCoords, i_fl, j_fl, n_fl, height);
		}

	// bind vertices
	glGenVertexArrays(1, &this->vertexArray);
	glBindVertexArray(this->vertexArray);
	glGenBuffers(1, &this->vertexBuffer);
	glGenBuffers(1, &this->textureCoordBuffer);
	glGenBuffers(1, &this->sunTexCoordBuffer);

	glBindBuffer(GL_ARRAY_BUFFER, this->vertexBuffer);
	// glBufferData(GL_ARRAY_BUFFER, sizeof(gridVertices), gridVertices, GL_STATIC_DRAW);
	glBufferData(GL_ARRAY_BUFFER, vertices.size() * sizeof(GLfloat), vertices.data(), GL_STATIC_DRAW);

	//Bind data from buffer to the first shader layout (xyz)
	glEnableVertexAttribArray(0);
	// glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 5 * sizeof(GLfloat), 0);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(GLfloat), 0);
	glDisableVertexAttribArray(0);

	glBindBuffer(GL_ARRAY_BUFFER, this->textureCoordBuffer);
	// glBufferData(GL_ARRAY_BUFFER, sizeof(gridVertices), gridVertices, GL_STATIC_DRAW);
	glBufferData(GL_ARRAY_BUFFER, textureCoords.size() * sizeof(GLfloat), textureCoords.data(), GL_DYNAMIC_DRAW);

	//Bind data from buffer to the first shader layout (uv)
	glEnableVertexAttribArray(1);
	// glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 5 * sizeof(GLfloat), 0);
	glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 2 * sizeof(GLfloat), 0);
	glDisableVertexAttribArray(1);

	glBindBuffer(GL_ARRAY_BUFFER, this->sunTexCoordBuffer);
	glBufferData(GL_ARRAY_BUFFER, textureCoords.size() * sizeof(GLfloat), textureCoords.data(), GL_DYNAMIC_DRAW);
	glEnableVertexAttribArray(2);
	glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, 2 * sizeof(GLfloat), nullptr);

	std::cout << "Size texture vector: " << textureCoords.size() << std::endl;
}

void Plane::Draw(GLuint shaderProgram, Camera& cam, int N, GLuint textureInt)
{
	glUseProgram(shaderProgram);

	if (this->texture == 1 && textureInt == 1)
	{
		glActiveTexture(GL_TEXTURE0);
		glBindTexture(GL_TEXTURE_2D, this->texture);
	}
	else if (this->texture == 2 && textureInt == 2)
	{
		glActiveTexture(GL_TEXTURE1);
		glBindTexture(GL_TEXTURE_2D, this->texture);
	}
	else
		return;

	glUniformMatrix4fv(
		glGetUniformLocation(shaderProgram, "viewMatrix"),
		1,
		GL_FALSE,
		glm::value_ptr(cam.ViewMatrix() * glm::scale(glm::mat4(1.0), glm::vec3(10.0 / N, 1, 10.0 / N)))
	);
	glUniformMatrix4fv(
		glGetUniformLocation(shaderProgram, "projectionMatrix"),
		1,
		GL_FALSE,
		glm::value_ptr(cam.ProjectionMatrix())
	);
	glUniform3f(
		glGetUniformLocation(shaderProgram, "texOffset"),
		cam.light.x,
		cam.light.y,
		cam.light.z
	);

	glUniform1i(glGetUniformLocation(shaderProgram, "groundTex"), 0);
	glUniform1i(glGetUniformLocation(shaderProgram, "sunTex"), 1);

	glBindVertexArray(this->vertexArray);
	
	glEnableVertexAttribArray(0);
	glEnableVertexAttribArray(1);
	glDrawArrays(GL_TRIANGLES, 0, planeVertices.size());
	glDisableVertexAttribArray(1);
	glDisableVertexAttribArray(0);
}
