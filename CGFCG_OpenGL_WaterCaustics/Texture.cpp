#include "Texture.h"
#include <iostream>

GLuint loadTexture(const char *texImagePath, const bool sunTex)
{
	GLuint texture;

	FIBITMAP* bitmap = FreeImage_Load(
		FreeImage_GetFileType(texImagePath, 0),
		texImagePath);

	FIBITMAP* pImage;

	pImage = FreeImage_ConvertTo32Bits(bitmap);

	if (!sunTex) {
		glActiveTexture(GL_TEXTURE0);
		glGenTextures(1, &texture);
		glBindTexture(GL_TEXTURE_2D, texture);
	}
	else {
		glActiveTexture(GL_TEXTURE1);
		glGenTextures(1, &texture);
		glBindTexture(GL_TEXTURE_2D, texture);
	}
	
	
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA8, FreeImage_GetWidth(pImage), FreeImage_GetHeight(pImage),
		0, GL_BGRA, GL_UNSIGNED_BYTE, static_cast<void*>(FreeImage_GetBits(pImage)));

	FreeImage_Unload(bitmap);
	FreeImage_Unload(pImage);

	return texture;
}
