#ifndef TEXTURE_H_
#define TEXTURE_H_

#include <GL/glew.h>
#include <SOIL2.h>
#include <FreeImage.h>

GLuint loadTexture(const char *texImagePath, const bool caustTex);

#endif