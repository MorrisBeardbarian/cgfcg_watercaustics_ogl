#ifndef SHADER_H_
#define SHADER_H_

#include <GL/glew.h>

GLuint loadShader(const char* vertexLocation, const char* fragmentLocation);
#endif