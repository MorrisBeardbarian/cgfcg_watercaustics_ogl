#define _CRT_SECURE_NO_WARNINGS
#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include <iostream>
#include <SOIL2.h>

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

#include "Plane.h"
#include "Shader.h"
#include "Texture.h"
#include "GameTime.h"
#include "Utils.h"
#include "Point.cpp"
#include "Wave.h"

#pragma region Variables

GLFWwindow* window;

GameTime gameTime;

GLuint groundPlaneShader;
GLuint waterSurfaceShader;
GLuint causticsShader;

GLuint groundTex;
GLuint waveTex;
GLuint sunTex;

GLuint causticsVertexBuffer;
GLuint causticsVertexArray;


Plane* groundPlane;
Plane* causticsPlane;
Wave* wave;

Camera cam;

glm::vec2 mousePosLast;

// Mesh resolution
float L_x = 1000;
float L_z = 1000;

#define MESH_RESOLUTION 64 /* 64 */
#define QUADSIZE L_x / MESH_RESOLUTION

int N = MESH_RESOLUTION;
int M = MESH_RESOLUTION;

int indexSize;

#define IOR 1.3333 //index of refraction 

//change this to make the waves calmer or more agitated
float A = 3e-7f;
// Wind speed
float V = 30;
// Wind direction
vec2 omega(1, 1);

GLuint WIDTH = 1280, HEIGHT = 720;


// Deltatime
GLfloat deltaTime = 0.0f;	// Time between current frame and last frame
GLfloat lastFrame = 0.0f;  	// Time of last frame

glm::vec3* vertexArray;
GLuint waveSurfaceVAO = 0;
GLuint waveSurfaceVBO, waveEBO;

float wTime = 0;
Wave *wave_model;

int nWaves = 60;
glm::vec4* waveParams;

float height;

float heightMax = 0;
float heightMin = 0;

// CAUSTIC TEXTURE
#define TEXTURE_OPAQUE 0
#define TEXTURE_OPACITY 1
#define TEXTURE_BLEND 2
#define TEXTURE_BLEND_ADD 3
unsigned int translationtable;
int allocatedmemory;
int type;

// wireframe
bool wireframeModeActive;

#pragma endregion

#pragma region Function Headers

void setupWindowAndContext();
int main(int argc, char * argv[]);
void initBufferObjects();

void update();
void render();

float yAt(int x, int z);

#pragma endregion

// print list of keyboard short-cuts
void printInstructions() {
	std::cout << "-----------------------------------------" << std::endl;
	std::cout << "Here is a list of the keyboard controls: " << std::endl;

	std::cout << "WASD - Move " << std::endl;
	std::cout << "Arrow keys - Rotate camera" << std::endl;
	std::cout << "Q - Toggle wireframe mode" << std::endl;
	std::cout << "E - Toggle textured mode" << std::endl;
	std::cout << "-----------------------------------------" << std::endl;
}

// delete buffers
void CleanUp() {
	glDisable(GL_DEPTH_TEST);
	glDisable(GL_BLEND);

	glDeleteVertexArrays(1, &waveSurfaceVAO);
	glDeleteBuffers(1, &waveSurfaceVBO);
	glDeleteBuffers(1, &waveEBO);
}

int main(int argc, char* argv[])
{
	// set up window
	setupWindowAndContext();

	// set background color
	glClearColor(1.0f, 1.0f, 1.0f, 0.75f);

	// set blend mode
	glEnable(GL_DEPTH_TEST);
	glDepthMask(GL_TRUE);

	// load in shaders
	groundPlaneShader = loadShader("Shaders/groundPlaneVertex.glsl", "Shaders/groundPlaneFragment.glsl");
	waterSurfaceShader = loadShader("Shaders/waterSurfaceVertex.glsl", "Shaders/waterSurfaceFragment.glsl");
	causticsShader = loadShader("Shaders/causticsPlaneVertex.glsl", "Shaders/causticsPlaneFragment.glsl");

	// Load in textures
	groundTex = loadTexture("Resources/floorTex.jpg", false);
	sunTex = loadTexture("Resources/sun.png", true);

	// build ground plane
	groundPlane = new Plane(-1.0f, N, groundTex);
	causticsPlane = new Plane(-0.95f, N, sunTex);

	// set camera position and light position
	cam.position = glm::vec3(-1.0f, 5.0f, -2.5f);
	cam.light = glm::vec3(0.0f, 0.0f, 0.0f);

	// set the rotation of the camera
	cam.rotation = glm::vec3(-173, -142, 0);

	wTime = 0;
	initBufferObjects();

	wave_model = new Wave(N, M, L_x, L_z, omega, V, A, 1);

	printInstructions();

	while (!glfwWindowShouldClose(window))
	{
		update();
		render();
	}

	CleanUp();

	return 0;
}

// add a quad with caustics uv
void addNewQuadCaustics(std::vector<GLfloat>& vector, float i, float j, float u, float v, float height) {
	
	vector.insert(vector.cend(), {
	  i, height, j, u, v,
	  (i + 1), height, j, u, v,
	  i, height, (j + 1), u, v,

	  (i + 1), height, j, u, v,
	  i, height, (j + 1), u, v,
	  (i + 1), height, (j + 1), u, v
	});
}


// here's the main stuff for the caustic
void sample_caustic(float xi, float zi, float &u, float &v) {

	float lower_bound = -2.5f;
	float upper_bound = 4.5f;
	float interval = 7.0f;


	Point p(xi, 0, zi); //create point p
	p.y = yAt(xi, zi); //set the Y of the point p
	Point q(xi + QUADSIZE, 0, zi); //create point q //TODO: why + QUADSIZE?
	q.y = yAt(xi + 1, zi); //set the Y of the point q
	Point r(xi, 0, zi + QUADSIZE); //create point r //TODO: why + QUADSIZE?
	r.y = yAt(xi, zi + 1); //set the Y of the point r

	Point e1;
	e1 = q - p;
	Point e2;
	e2 = r - p;	//TODO: what are these?
	Point n;
	n = e1 ^ e2;	// the normal above the sampling point 
	u = 0; v = 0;

	//Unnecessary numrays for as numrays is always 1
	int numrays = 1;
	for (int i = 0; i < numrays; i++)
	{
		float alpha = 6.28*(float)i / numrays; //0
		float rad = (float)i / numrays; //0

		float xf = rad * cos(alpha) / 2.0; //0
		float zf = rad * sin(alpha) / 2.0; //0

		Point incident(xf, 1, zf); //point of incidence

		float c = incident * n; //point of incidence multiplied by the normal
		float sq = 1 + (IOR*IOR*(c*c - 1)); // inverse snells law

		if (sq > 0)
			sq = glm::sqrt(sq);
		else
			sq = 0;
		Point transmitted;
		transmitted = incident * IOR + n * (IOR*c - sq);
		transmitted.normalize();

		u += transmitted.x;
		v += transmitted.z;
	}
	u = 4 * (u / numrays) + 0.5;
	v = 4 * (v / numrays) + 0.5;

	// normalizing
	// first moving into positivity, then normalizing
	u = (u + 2.5f) / interval;
	v = (v + 2.55f) / interval;
}


void compute_caustic(Plane* causticsPlane)
{
	std::vector<GLfloat> sampledTextureCoords;

	float maxU = 0.0f;
	float maxV = 0.0f;

	float minU = 0.0f;
	float minV = 0.0f;

	for (int i = 0; i < N; i++)
		for (int j = 0; j < N; j++)
		{

			float u1, v1;
			float u2, v2;
			float u3, v3;
			float u4, v4;

			sample_caustic(i, j, u1, v1);
			sample_caustic(i + 1, j, u2, v2);
			sample_caustic(i, j + 1, u3, v3);
			sample_caustic(i + 1, j + 1, u4, v4);

			sampledTextureCoords.insert(sampledTextureCoords.cend(), { u1, v1, u2, v2, u4, v4, u2, v2, u4, v4, u3, v3 });

		}

	// std::cout << "Size sampled texture vector: " << sampledTextureCoords.size() << std::endl;

	glBindBuffer(GL_ARRAY_BUFFER, causticsPlane->sunTexCoordBuffer);
	// glBufferData(GL_ARRAY_BUFFER, sizeof(gridVertices), gridVertices, GL_STATIC_DRAW);
	glBufferData(GL_ARRAY_BUFFER, sampledTextureCoords.size() * sizeof(GLfloat), sampledTextureCoords.data(), GL_DYNAMIC_DRAW);
	
}


void initBufferObjectsGround()
{
	indexSize = (N - 1) * (M - 1) * 6;
	GLuint *indices = new GLuint[indexSize];

	int p = 0;

	for (int j = 0; j < N - 1; j++)
		for (int i = 0; i < M - 1; i++)
		{
			indices[p++] = i + j * N;
			indices[p++] = (i + 1) + j * N;
			indices[p++] = i + (j + 1) * N;

			indices[p++] = (i + 1) + j * N;
			indices[p++] = (i + 1) + (j + 1) * N;
			indices[p++] = i + (j + 1) * N;
		}

	// Element buffer object
	glGenVertexArrays(1, &waveSurfaceVAO);
	glBindVertexArray(waveSurfaceVAO);
	glGenBuffers(1, &waveSurfaceVBO);
	glGenBuffers(1, &waveEBO);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, waveEBO);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, indexSize * sizeof(GLuint), indices, GL_STATIC_DRAW);

	delete[] indices;
}

// create vertices for wave 
void initBufferObjects()
{
	indexSize = (N - 1) * (M - 1) * 6;
	GLuint *indices = new GLuint[indexSize];

	int p = 0;

	for (int j = 0; j < N - 1; j++)
		for (int i = 0; i < M - 1; i++)
		{
			indices[p++] = i + j * N;
			indices[p++] = (i + 1) + j * N;
			indices[p++] = i + (j + 1) * N;

			indices[p++] = (i + 1) + j * N;
			indices[p++] = (i + 1) + (j + 1) * N;
			indices[p++] = i + (j + 1) * N;
		}

	// Element buffer object
	glGenVertexArrays(1, &waveSurfaceVAO);
	glBindVertexArray(waveSurfaceVAO);
	glGenBuffers(1, &waveSurfaceVBO);
	glGenBuffers(1, &waveEBO);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, waveEBO);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, indexSize * sizeof(GLuint), indices, GL_STATIC_DRAW);

	delete[] indices;
}

float yAt(int x, int z) {
	int index = z * N + x;
	return ((wave_model->heightField)[index]).y;
}

// _WAVE_
// Build the mesh using the height provided by the algorithm.
void buildTessendorfWaveMesh(float wTime, float heightMin, float heightMax, GLuint surfaceVAO, GLuint surfaceVBO)
{
	int nVertex = N * M;

	wave_model->buildField(wTime);
	vec3* heightField = wave_model->heightField;
	vec3* normalField = wave_model->normalField;

	int p = 0;

	for (int i = 0; i < N; i++)
		for (int j = 0; j < M; j++)
		{
			int index = j * N + i;

			if (heightField[index].y > heightMax) heightMax = heightField[index].y;
			else if (heightField[index].y < heightMin) heightMin = heightField[index].y;
		}


	glBindVertexArray(surfaceVAO);
	glBindBuffer(GL_ARRAY_BUFFER, surfaceVBO);

	int fieldArraySize = sizeof(glm::vec3) * nVertex;
	glBufferData(GL_ARRAY_BUFFER, fieldArraySize * 2, NULL, GL_STATIC_DRAW);

	// Copy height to buffer
	glBufferSubData(GL_ARRAY_BUFFER, 0, fieldArraySize, heightField);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(GLfloat), (GLvoid*)0);
	glEnableVertexAttribArray(0);

	// Copy normal to buffer
	glBufferSubData(GL_ARRAY_BUFFER, fieldArraySize, fieldArraySize, normalField);
	glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(GLfloat), (GLvoid*)fieldArraySize);
	glEnableVertexAttribArray(1);
	//std::cout << yAt(1, 1) << std::endl;
}

// _WAVE_
void drawWater(GLuint shaderProgram, Camera& cam, float &wTime, float &heightMin,
	float &heightMax, GLuint &surfaceVAO, GLuint &surfaceVBO) {

	wTime += 0.20;
	float modelScale = 0.013f;

	// Use corresponding shader when setting uniforms/drawing objects
	glUseProgram(shaderProgram);

	// Get the uniform locations
	GLint modelLoc = glGetUniformLocation(shaderProgram, "model");
	GLint viewLoc = glGetUniformLocation(shaderProgram, "view");
	GLint projLoc = glGetUniformLocation(shaderProgram, "projection");
	GLint camPosLoc = glGetUniformLocation(shaderProgram, "cameraPosition");
	// Pass the matrices to the shader
	glm::mat4 view;
	view = cam.ViewMatrix();
	glm::mat4 projection = cam.ProjectionMatrix();
	glUniformMatrix4fv(viewLoc, 1, GL_FALSE, glm::value_ptr(view));
	glUniformMatrix4fv(projLoc, 1, GL_FALSE, glm::value_ptr(projection));
	if (camPosLoc >= 0)
		glUniform3fv(camPosLoc, 1, glm::value_ptr(cam.position));

	// ===== Draw Model =====
	glBindVertexArray(surfaceVAO);

	glm::mat4 model;
	model = glm::mat4();
	model = glm::scale(model, glm::vec3(modelScale));	// Scale the surface
	model = glm::translate(model, glm::vec3(500.0f, 0.0f, 500.0f));
	glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));

	//std::cout << normalField->x << ", " << normalField->y << ", " << normalField->z << std::endl;

	glDrawElements(GL_TRIANGLES, indexSize, GL_UNSIGNED_INT, 0);
	glBindVertexArray(0);
}

void setupWindowAndContext()
{
	wireframeModeActive = false;

	if (!glfwInit())
	{
		std::cout << "GLFW Failed to Initialize!" << std::endl;
		exit(EXIT_FAILURE);
	}
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
	glfwWindowHint(GLFW_OPENGL_DEBUG_CONTEXT, GLFW_TRUE);
	window = glfwCreateWindow(WIDTH, HEIGHT, "OpenGL - Water Caustics", NULL, NULL);
	glfwMakeContextCurrent(window);

	// blending
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA); 
	glEnable(GL_BLEND); 
	glDepthMask(GL_FALSE);
	glDepthFunc(GL_LEQUAL);

	glewExperimental = true;
	if (glewInit() != GLEW_OK)
	{
		std::cout << "Glew Failed to Initialize!" << std::endl;
		exit(EXIT_FAILURE);
	}
}

void update()
{
	gameTime.Update();

	cam.light -= glm::vec3(0.1f, 0.2f, 0) * gameTime.GetDeltaTimeSecondsF();
	cam.UpdateRotation(window, 20.0f * gameTime.GetDeltaTimeSecondsF());

	//Camera Rotations
	double xpos, ypos;
	glfwGetCursorPos(window, &xpos, &ypos);
	mousePosLast = glm::vec2(xpos, ypos); //tried camera rotation with mouse

	//Camera Translations
	glm::vec3 vel = glm::vec3(0, 0, 0);
	if (glfwGetKey(window, GLFW_KEY_A) == GLFW_PRESS)
	{
		vel += glm::vec3(-1, 0, 0);
	}
	if (glfwGetKey(window, GLFW_KEY_D) == GLFW_PRESS)
	{
		vel += glm::vec3(1, 0, 0);
	}
	if (glfwGetKey(window, GLFW_KEY_W) == GLFW_PRESS)
	{
		vel += glm::vec3(0, 0, 1);
	}
	if (glfwGetKey(window, GLFW_KEY_S) == GLFW_PRESS)
	{
		vel += glm::vec3(0, 0, -1);
	}
	if (glfwGetKey(window, GLFW_KEY_Q) == GLFW_PRESS)
	{
		wireframeModeActive = true;
	}
	if (glfwGetKey(window, GLFW_KEY_E) == GLFW_PRESS)
	{
		wireframeModeActive = false;
	}

	if(wireframeModeActive) 
		glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
	else
		glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);

	vel *= gameTime.GetDeltaTimeSecondsF();
	cam.Translate(vel);

	glfwPollEvents();
}

void render()
{
	glClearColor(1.0f, 1.0f, 1.0f, 0.5f);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	buildTessendorfWaveMesh(wTime, heightMin, heightMax, waveSurfaceVAO, waveSurfaceVBO);
	
	
	compute_caustic(groundPlane);
	groundPlane->Draw(groundPlaneShader, cam, N, groundTex);
	drawWater(waterSurfaceShader, cam, wTime, heightMin, heightMax, waveSurfaceVAO, waveSurfaceVBO);

	glfwSwapBuffers(window);
}
