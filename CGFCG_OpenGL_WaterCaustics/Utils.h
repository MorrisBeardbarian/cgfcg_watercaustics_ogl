#include <iostream>
#include <glm/glm.hpp>
#include <glm/gtx/string_cast.hpp>

// Print a GLM Vector3
void printVector(glm::vec3 vector);
void printVector(glm::vec3 vector, const char* message);

// print a GLM Matrix
void printMatrix(glm::mat4 matrix);
void printMatrix(glm::mat4 matrix, const char* message);