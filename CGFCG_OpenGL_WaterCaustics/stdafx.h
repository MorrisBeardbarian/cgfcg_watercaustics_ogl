#pragma once

#define _USE_MATH_DEFINES
#define M_PI 3.14159265358979323846

#include <cmath>
#include <iostream>

#include <glm/glm.hpp>
