#include "Utils.h"

void printVector(glm::vec3 vector)
{
	std::cout << "X: " << vector.x << " Y: " << vector.y << " Z: " << vector.z << std::endl;
}

void printVector(glm::vec3 vector, const char* message)
{
	std::cout << message << " X: " << vector.x << " Y: " << vector.y << " Z: " << vector.z << std::endl;
}

void printMatrix(glm::mat4 matrix)
{
	std::cout << glm::to_string(matrix) << std::endl;
} 

void printMatrix(glm::mat4 matrix, const char * message)
{
	std::cout << message << std::endl;
	std::cout << glm::to_string(matrix) << std::endl;
}



//int load(char *name)
/*-------------------------------------------------------------------------------*/
/* loads a texture manager from a description file                               */
/* file format is:                                                               */
/* numtextures [NUMTEXTURES]                                                     */
/* TEXTURE [POSITION] [FILENAME] OPAQUE for regular opaque textures              */
/* TEXTURE [POSITION] [FILENAME] BILLBOARD [KEYCOLOR] for billboards             */
/* TEXTURE [POSITION] [FILENAME] TRANSLUCENT LUMINOSITY for rgb-based            */
/* TEXTURE [POSITION] [FILENAME] TRANSLUCENT FIXED [ALPHA]                       */
/* TEXTURE [POSITION] [FILENAME] TRANSLUCENT LINEAR [COLOR]                      */
/*-------------------------------------------------------------------------------*/
/*
{
	text t(name);
	int i;

	// numtextures
	numtextures = t.countword("#TEXTURE");
	translationtable = new unsigned int[numtextures];
	type = new int[numtextures];
	glGenTextures(numtextures, translationtable);
	texdata = new texture[numtextures];
	// load each texture object
	for (i = 0; i < numtextures; i++)
	{
		t.seek("#TEXTURE");
		char *name = t.getword();
		int newchunksize = texdata[i].loadtga(name, translationtable[i]);
		char *cl = t.getword();
		type[i] = TEXTURE_OPAQUE;
		if (!strcmp(cl, "OPACITY")) type[i] = TEXTURE_OPACITY;
		if (!strcmp(cl, "BLEND")) type[i] = TEXTURE_BLEND;
		if (!strcmp(cl, "ADDITIVEBLEND")) type[i] = TEXTURE_BLEND_ADD;
		delete cl;
		if (newchunksize == -1)
		{
			return -1;
		}
		else allocatedmemory += newchunksize;
		delete name;
	}
	return allocatedmemory;
}
*/