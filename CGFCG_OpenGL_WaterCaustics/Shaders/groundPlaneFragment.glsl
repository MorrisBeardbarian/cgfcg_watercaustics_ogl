#version 330

#ifdef GL_ES
precision mediump float;
#endif

varying vec2 texCoord;
varying vec2 sunTexCoord;
uniform sampler2D groundTex;
uniform sampler2D sunTex;

void main()
{
	gl_FragColor = mix(texture(groundTex, texCoord), texture(sunTex, sunTexCoord), 0.6);
}