#version 330

#ifdef GL_ES
precision mediump float;
#endif

uniform vec3 cameraPostion;

in vec3 Normal;
in vec3 FragPos;

const vec3 sunPos = vec3(0, 10, 0);

void main()
{
	vec3 new_color = vec3((233.0/255.0), (230.0/255.0), (211.0/255.0));
	// vec3 new_color = vec3((64.0/255.0), (164.0/255.0), (223.0/255.0));

	vec3 CameraDirection = -normalize(cameraPostion - FragPos);
	vec3 lightDir = -normalize(sunPos - FragPos);

	float difuse = max(dot(Normal, lightDir), 0.0);
	vec3 reflectDir = reflect(lightDir, Normal);

	float specular = pow(max(dot(CameraDirection, reflectDir), 0.0), 16);

    float NdotCD = max(dot(Normal, CameraDirection), 0.0);
    gl_FragColor = vec4((difuse + specular * 10) *new_color, 0.3);
}