#ifndef _Point_INC
#define _Point_INC

/*
Written by DSC aprox. 1996
1.7.98 Translation into good-looking C++
*/

#include "text.h"

class Point
        {
        public:
                double x,y,z;    /* oops... this makes rendering faster! */

                Point();
                Point(double,double,double);
                Point(text &);
                Point(Point &);

                void create(double,double,double);
                void load(text &);

                Point operator+(Point);       // addition
                Point operator-(Point);       // substraction
                int operator==(Point);        // comparison
                int operator!=(Point);        // negated comparison                        
                void operator=(Point);        // copy
                Point operator*(double);      // scaling
				Point operator/(double);	  // inv. scaling
                double operator*(Point);      // dot product
                Point operator^(Point);       // cross product

                Point interpolate(Point,double,Point,double);
                double modulo();
                double modulosq();
				double distance(Point);
				double distanceman(Point);
				double distancemanxz(Point);

                void normalize();
                void negate();
                int into(Point, Point);
				float distancePointLine(Point,Point);//distance from Point this to line ab

   				void rotatex(double);
				void rotatey(double);
				void rotatey(float,float); //rotatey passing sin(ang) and cos(ang) values
				void rotatez(double);

				bool infrustum(Point,double,double);	// viewer,yaw of viewer,aperture,caller is Point to test

				~Point();
        };

#endif





